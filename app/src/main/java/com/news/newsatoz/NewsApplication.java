package com.news.newsatoz;


import androidx.multidex.MultiDexApplication;

import com.news.newsatoz.retrofit.ApiClient;
import com.news.newsatoz.retrofit.NewsApi;

public class NewsApplication extends MultiDexApplication {

    private static NewsApplication instance;

    private NewsApi mNewsApi;

    public static NewsApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public NewsApi getRestApi() {
        if (mNewsApi == null) {
            mNewsApi = ApiClient.getClient().create(NewsApi.class);
        }
        return mNewsApi;
    }
}


