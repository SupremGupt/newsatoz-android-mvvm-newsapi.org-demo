package com.news.newsatoz.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.news.newsatoz.BR;
import com.news.newsatoz.R;
import com.news.newsatoz.activities.MainActivity;
import com.news.newsatoz.activities.SearchActivity;
import com.news.newsatoz.databinding.ItemHeaderBinding;
import com.news.newsatoz.databinding.ItemListFeedBinding;
import com.news.newsatoz.databinding.NetworkItemBinding;
import com.news.newsatoz.model.Article;
import com.news.newsatoz.utils.NetworkState;

public class NewsMainListAdapter extends PagedListAdapter<Article, RecyclerView.ViewHolder> {

    private ViewModel viewModel;

    private static final int TYPE_PROGRESS = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_HEADER = 2;

    AppCompatActivity mContext;
    private NetworkState networkState;

    public NewsMainListAdapter(AppCompatActivity context, ViewModel viewModel) {
        super(Article.DIFF_CALLBACK);
        this.mContext = context;
        this.viewModel = viewModel;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_PROGRESS) {
            NetworkItemBinding headerBinding = NetworkItemBinding.inflate(layoutInflater, parent, false);
            NetworkStateItemViewHolder viewHolder = new NetworkStateItemViewHolder(headerBinding);
            return viewHolder;
        } else if (viewType == TYPE_HEADER) {
            ItemHeaderBinding itemHeaderBinding = ItemHeaderBinding.inflate(layoutInflater, parent, false);
            HeaderItemViewHolder viewHolder = new HeaderItemViewHolder(itemHeaderBinding);
            return viewHolder;
        } else {
            ItemListFeedBinding itemBinding = ItemListFeedBinding.inflate(layoutInflater, parent, false);
            ArticleItemViewHolder viewHolder = new ArticleItemViewHolder(itemBinding);
            return viewHolder;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleItemViewHolder) {
            ((ArticleItemViewHolder) holder).bind(getItem(position), viewModel);
        } else if (holder instanceof HeaderItemViewHolder) {
            ((HeaderItemViewHolder) holder).bind(mContext);
        } else {
            ((NetworkStateItemViewHolder) holder).bindView(networkState);
        }
    }


    private boolean hasExtraRow() {
        if (networkState != null && networkState != NetworkState.LOADED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (hasExtraRow() && position == getItemCount() - 1) {
            return TYPE_PROGRESS;
        } else if ((position == 0 || position == 21)) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    public void setNetworkState(NetworkState newNetworkState) {
        NetworkState previousState = this.networkState;
        boolean previousExtraRow = hasExtraRow();
        this.networkState = newNetworkState;
        boolean newExtraRow = hasExtraRow();
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(getItemCount());
            } else {
                notifyItemInserted(getItemCount());
            }
        } else if (newExtraRow && previousState != newNetworkState) {
            notifyItemChanged(getItemCount() - 1);
        }
    }


    public static class ArticleItemViewHolder extends RecyclerView.ViewHolder {
        final ItemListFeedBinding binding;

        ArticleItemViewHolder(ItemListFeedBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Article obj, ViewModel viewModel) {
            binding.setVariable(BR.obj, obj);
            binding.setVariable(BR.viewModel, viewModel);
            binding.executePendingBindings();
        }

    }

    public static class HeaderItemViewHolder extends RecyclerView.ViewHolder {
        ItemHeaderBinding mItemHeaderBinding;

        public HeaderItemViewHolder(ItemHeaderBinding binding) {
            super(binding.getRoot());
            mItemHeaderBinding = binding;
        }

        public void bind(AppCompatActivity mContext) {
            if (mContext instanceof MainActivity) {
                if (getAdapterPosition() == 0) {
                    mItemHeaderBinding.textHeader.setText(R.string.top_headline_for_you);
                } else {
                    mItemHeaderBinding.textHeader.setText(R.string.important_for_you);
                }
            } else if (mContext instanceof SearchActivity) {
                if (getAdapterPosition() == 0) {
                    mItemHeaderBinding.textHeader.setText(String.format(mContext.getString(R.string.top_search_result), ((SearchActivity) mContext).getSearchQuery()));
                } else {
                    mItemHeaderBinding.textHeader.setText(String.format(mContext.getString(R.string.more_search_results), ((SearchActivity) mContext).getSearchQuery()));
                }
            }
        }


    }


    public class NetworkStateItemViewHolder extends RecyclerView.ViewHolder {

        private NetworkItemBinding binding;

        public NetworkStateItemViewHolder(NetworkItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindView(NetworkState networkState) {
            if (networkState != null && networkState.getStatus() == NetworkState.Status.RUNNING) {
                binding.progressBar.setVisibility(View.VISIBLE);
            } else {
                binding.progressBar.setVisibility(View.GONE);
            }

            if (networkState != null && networkState.getStatus() == NetworkState.Status.FAILED) {
                binding.errorMsg.setVisibility(View.VISIBLE);
                binding.errorMsg.setText(networkState.getMsg());
            } else {
                binding.errorMsg.setVisibility(View.GONE);
            }
        }
    }
}
