package com.news.newsatoz.retrofit;

import com.news.newsatoz.model.NewsData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApi {
    @GET("everything")
    Call<NewsData> getAllList(@Query("q") String query, @Query("sortBy") String sortBy, @Query("from") String fromDate, @Query("to") String toDate, @Query("apikey") String apiKey,
            @Query("page") long page, @Query("pageSize") int pageSize);

    @GET("top-headlines")
    Call<NewsData> getTopHeadLines(@Query("country") String country, @Query("sortby") String sortyBy, @Query("apikey") String apiKey, @Query("page") long page);

    @GET("everything")
    Call<NewsData> getAllList(@Query("q") String query, @Query("sortBy") String sortBy, @Query("from") String fromDate, @Query("to") String toDate, @Query("apikey") String apiKey,
            @Query("page") long page);
}

