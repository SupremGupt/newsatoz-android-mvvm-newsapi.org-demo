package com.news.newsatoz.retrofit.calls;

import com.news.newsatoz.model.IpLocation;
import com.news.newsatoz.retrofit.IpLocationApi;
import com.news.newsatoz.retrofit.IpStackApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationCall {

    public static final String IP_STACK_ACCESS_KEY = "2db278da2a3ef4d88bd9d25ba0da2035";
    private CallBack callBack;

    private LocationCall() {

    }

    public LocationCall(CallBack callBack) {
        this.callBack = callBack;
    }

    public interface CallBack {
        public void onSuccess(IpLocation ipLocation);

        public void onFailure();
    }


    public void sendRequest() {
        IpLocationApi api = IpStackApiClient.getClient().create(IpLocationApi.class);
        api.getIpLocation(IP_STACK_ACCESS_KEY, 1).enqueue(new Callback<IpLocation>() {
            @Override
            public void onResponse(Call<IpLocation> call, Response<IpLocation> response) {
                callBack.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<IpLocation> call, Throwable t) {
                callBack.onFailure();
            }
        });
    }
}
