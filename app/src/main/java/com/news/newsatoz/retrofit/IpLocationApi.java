package com.news.newsatoz.retrofit;

import com.news.newsatoz.model.IpLocation;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IpLocationApi {
    @GET("check")
    Call<IpLocation> getIpLocation(@Query("access_key") String access_key, @Query("format") int format);
}
