/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.news.newsatoz.viewmodel;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.news.newsatoz.model.Article;
import com.news.newsatoz.utils.AppUtils;

import androidx.databinding.BindingAdapter;

public class ArticleDetailViewModel {

    private Article mArticle;
    private String imageUrl;
    private Context context;

    public ArticleDetailViewModel(Article article, Context context) {
        this.mArticle = article;
        this.context = context;
        imageUrl = article.getUrlToImage();
    }

    public String getTitle() {
        return mArticle.getTitle();
    }

    public String getContent() {
        return !TextUtils.isEmpty(mArticle.getContent()) ? mArticle.getContent() : mArticle.getDescription();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void onItemClick(View view) {
        if (!TextUtils.isEmpty(mArticle.getUrl())) {
            AppUtils.intentUtil(context, mArticle.getUrl());
        } else {
            Toast.makeText(context, "Url Empty. Cant show", Toast.LENGTH_LONG).show();
        }
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext()).load(imageUrl).into(view);
    }
}
