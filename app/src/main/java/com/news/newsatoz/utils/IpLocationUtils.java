package com.news.newsatoz.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;

import com.news.newsatoz.NewsApplication;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class IpLocationUtils {


    public static String getLocationString() {
        return getUserCityName() + " " + getUserRegionOrStateName() + " " + getUserCountryName();
    }

    public static void setUserCityName(String cityName) {
        if (TextUtils.isEmpty(cityName))
            return;
        AppUtils.getTinyDb().putString(Constants.PREF_KEY_CITY, cityName);
    }

    public static void setUserRegionName(String regionName) {
        if (TextUtils.isEmpty(regionName))
            return;
        AppUtils.getTinyDb().putString(Constants.PREF_KEY_REGION_NAME, regionName);
    }

    public static void setUserCountryName(String countryName) {
        if (TextUtils.isEmpty(countryName))
            return;
        AppUtils.getTinyDb().putString(Constants.PREF_KEY_COUNTRY, countryName);
    }

    public static void setUserIpAddress(String ip) {
        if (TextUtils.isEmpty(ip))
            return;
        AppUtils.getTinyDb().putString(Constants.PREF_KEY_LAST_USER_IP, ip);
    }

    public static void setUserCountryCode(String code) {
        if (TextUtils.isEmpty(code))
            return;
        AppUtils.getTinyDb().putString(Constants.PREF_KEY_USER_COUNTRY_CODE, code);
    }


    public static String getUserCityName() {
        return AppUtils.getTinyDb().getString(Constants.PREF_KEY_CITY);
    }

    public static String getUserRegionOrStateName() {
        return AppUtils.getTinyDb().getString(Constants.PREF_KEY_REGION_NAME);
    }

    public static String getUserCountryName() {
        return AppUtils.getTinyDb().getString(Constants.PREF_KEY_COUNTRY);
    }

    public static String getUserIPAddress() {
        return AppUtils.getTinyDb().getString(Constants.PREF_KEY_LAST_USER_IP);
    }

    public static String getUserCountryCode() {
        return AppUtils.getTinyDb().getString(Constants.PREF_KEY_USER_COUNTRY_CODE);
    }

    @NonNull
    public static String getDeviceIpAddress() {
        String actualConnectedToNetwork = null;
        ConnectivityManager connManager = (ConnectivityManager) NewsApplication.getInstance().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connManager != null) {
            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (mWifi.isConnected()) {
                actualConnectedToNetwork = getWifiIp();
            }
        }
        if (TextUtils.isEmpty(actualConnectedToNetwork)) {
            actualConnectedToNetwork = getNetworkInterfaceIpAddress();
        }
        if (TextUtils.isEmpty(actualConnectedToNetwork)) {
            actualConnectedToNetwork = "127.0.0.1";
        }
        return actualConnectedToNetwork;
    }

    @Nullable
    private static String getWifiIp() {
        final WifiManager mWifiManager = (WifiManager) NewsApplication.getInstance().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (mWifiManager != null && mWifiManager.isWifiEnabled()) {
            int ip = mWifiManager.getConnectionInfo().getIpAddress();
            return (ip & 0xFF) + "." + ((ip >> 8) & 0xFF) + "." + ((ip >> 16) & 0xFF) + "."
                    + ((ip >> 24) & 0xFF);
        }
        return null;
    }


    @Nullable
    public static String getNetworkInterfaceIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface networkInterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkInterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        String host = inetAddress.getHostAddress();
                        if (!TextUtils.isEmpty(host)) {
                            return host;
                        }
                    }
                }

            }
        } catch (Exception ex) {
            Log.e("IP Address", "getLocalIpAddress", ex);
        }
        return null;
    }
}
