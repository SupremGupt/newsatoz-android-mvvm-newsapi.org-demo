package com.news.newsatoz.utils;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.news.newsatoz.model.NewsData;

public class NewsDataHelper {

    private static NewsDataHelper mInstance;

    private NewsDataHelper() {

    }

    public static NewsDataHelper getInstance() {
        if (mInstance == null) {
            return new NewsDataHelper();
        }
        return mInstance;
    }


    public void setLastSaveNewsTime() {
        AppUtils.getTinyDb().putLong(Constants.PREF_MAIN_PAGE_FETCH_LAST_TIME, System.currentTimeMillis());
    }

    public void setLastSaveSearchTime() {
        AppUtils.getTinyDb().putLong(Constants.PREF_SEARCH_PAGE_FETCH_LAST_TIME, System.currentTimeMillis());
    }

    public boolean shouldGetSavedNewsData() {
        long lastSaveTime = AppUtils.getTinyDb().getLong(Constants.PREF_MAIN_PAGE_FETCH_LAST_TIME, 0L);
        if (System.currentTimeMillis() < (lastSaveTime + 4 * 60 * 60 * 1000) || !AppUtils.isNetworkConnected()) {
            return true;
        }
        return false;
    }

    public boolean shouldGetSavedSearchData() {
        long lastSaveTime = AppUtils.getTinyDb().getLong(Constants.PREF_SEARCH_PAGE_FETCH_LAST_TIME, 0L);
        if (System.currentTimeMillis() < (lastSaveTime + 4 * 60 * 60 * 1000) || !AppUtils.isNetworkConnected()) {
            return true;
        }
        return false;
    }


    public void saveNewsDataForHeadline(NewsData newsData) {
        if (newsData != null && newsData.getArticles() != null && newsData.getArticles().size() > 0) {
            String newsDataString = new Gson().toJson(newsData, NewsData.class);
            AppUtils.getTinyDb().putString(Constants.PREF_MAIN_PAGE_HEADLINE_RESULTS, newsDataString);
        }
    }


    public NewsData getSavedNewsForHeadline() {
        NewsData newsData = null;
        String result = AppUtils.getTinyDb().getString(Constants.PREF_MAIN_PAGE_HEADLINE_RESULTS);
        if (!TextUtils.isEmpty(result)) {
            newsData = new Gson().fromJson(result, NewsData.class);
        }
        return newsData;
    }

    public void saveNewsDataForPage(NewsData newsData, int page) {
        if (newsData != null && newsData.getArticles() != null && newsData.getArticles().size() > 0 && page > 0) {
            String newsDataString = new Gson().toJson(newsData, NewsData.class);
            AppUtils.getTinyDb().putString(Constants.PREF_MAIN_PAGE_RESULTS + String.valueOf(page), newsDataString);
        }
    }

    public NewsData getSavedNewsForPage(int page) {
        NewsData newsData = null;
        if (page > 0) {
            String result = AppUtils.getTinyDb().getString(Constants.PREF_MAIN_PAGE_RESULTS + String.valueOf(page));
            if (!TextUtils.isEmpty(result)) {
                newsData = new Gson().fromJson(result, NewsData.class);
            }
        }
        return newsData;
    }


    public void saveSearchData(NewsData newsData) {
        if (newsData != null && newsData.getArticles() != null && newsData.getArticles().size() > 0) {
            String newsDataString = new Gson().toJson(newsData, NewsData.class);
            AppUtils.getTinyDb().putString(Constants.PREF_SEARCH_PAGE_RESULTS, newsDataString);
        }
    }

    public NewsData getSavedSearchData() {
        NewsData newsData = null;
        String result = AppUtils.getTinyDb().getString(Constants.PREF_SEARCH_PAGE_RESULTS);
        if (!TextUtils.isEmpty(result)) {
            newsData = new Gson().fromJson(result, NewsData.class);
        }
        return newsData;
    }
}
