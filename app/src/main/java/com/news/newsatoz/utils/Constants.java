package com.news.newsatoz.utils;

public class Constants {


    public static final String PREF_NAME_LOCATION = "pref_name_location";
    public static final String PREF_KEY_CITY = "pref_key_city";
    public static final String PREF_KEY_COUNTRY = "pref_key_country";
    public static final String PREF_KEY_REGION_NAME = "pref_key_region_name";
    public static final String PREF_KEY_LAST_USER_IP = "PREF_KEY_LAST_USER_IP";
    public static final String PREF_KEY_USER_COUNTRY_CODE = "pref_user_country_code";


    public static final String PREF_MAIN_PAGE_RESULTS = "pref_main_page_result_";
    public static final String PREF_MAIN_PAGE_HEADLINE_RESULTS = "pref_main_page_headline_result";
    public static final String PREF_SEARCH_PAGE_RESULTS = "pref_search_page_result_";

    public static final String PREF_MAIN_PAGE_FETCH_LAST_TIME = "pref_last_main_page_fetch_time";
    public static final String PREF_SEARCH_PAGE_FETCH_LAST_TIME = "pref_last_search_page_fetch_time";


}
