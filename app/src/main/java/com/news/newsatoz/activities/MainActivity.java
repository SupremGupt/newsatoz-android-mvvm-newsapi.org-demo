package com.news.newsatoz.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.news.newsatoz.NewsApplication;
import com.news.newsatoz.R;
import com.news.newsatoz.adapter.NewsMainListAdapter;
import com.news.newsatoz.databinding.ActivityMainBinding;
import com.news.newsatoz.model.Article;
import com.news.newsatoz.viewmodel.FeedViewModel;

public class MainActivity extends AppCompatActivity {
    private NewsMainListAdapter adapter;
    private FeedViewModel feedViewModel;
    private ActivityMainBinding activityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupBindings(savedInstanceState);

    }

    private void setupBindings(Bundle savedInstanceState) {
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        feedViewModel = ViewModelProviders.of(this).get(FeedViewModel.class);
        feedViewModel.init("");

        activityBinding.rvTopHeadline.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new NewsMainListAdapter(this, feedViewModel);

        feedViewModel.getArticleLiveData().observe(this, pagedList -> {
            adapter.submitList(pagedList);
        });

        feedViewModel.getNetworkState().observe(this, networkState -> {
            adapter.setNetworkState(networkState);
        });

        activityBinding.rvTopHeadline.setAdapter(adapter);
        setupListClick();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setupListClick() {
        feedViewModel.getSelected().observe(this, new Observer<Article>() {
            @Override
            public void onChanged(Article results) {
                if (results != null) {
                    startActivity(ArticleDetailActivity.launchDetail(NewsApplication.getInstance(), new Gson().toJson(results, Article.class)));

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem searchItem = menu.findItem(R.id.searchBar);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, SearchActivity.class));
                return false;
            }
        });
        return true;
    }

}
