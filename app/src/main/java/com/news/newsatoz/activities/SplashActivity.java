package com.news.newsatoz.activities;

import android.content.Intent;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.news.newsatoz.R;
import com.news.newsatoz.model.IpLocation;
import com.news.newsatoz.retrofit.calls.LocationCall;
import com.news.newsatoz.utils.IpLocationUtils;

import androidx.appcompat.app.AppCompatActivity;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Launcher);
        Fabric.with(this, new Crashlytics());
        super.onCreate(savedInstanceState);

        LocationCall locationCall = new LocationCall(new LocationCall.CallBack() {
            @Override
            public void onSuccess(IpLocation ipLocation) {
                if (ipLocation != null) {
                    IpLocationUtils.setUserCityName(ipLocation.getCity());
                    IpLocationUtils.setUserRegionName(ipLocation.getRegion_name());
                    IpLocationUtils.setUserCountryName(ipLocation.getCountry_name());
                    IpLocationUtils.setUserIpAddress(ipLocation.getIp());
                    IpLocationUtils.setUserCountryCode(ipLocation.getCountry_code());
                }
                openMainActivity();
            }

            @Override
            public void onFailure() {
                openMainActivity();
            }
        });
        locationCall.sendRequest();
    }

    private void openMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
