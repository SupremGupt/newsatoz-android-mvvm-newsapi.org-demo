/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.news.newsatoz.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.news.newsatoz.R;
import com.news.newsatoz.databinding.ArticleDetailLayoutBinding;
import com.news.newsatoz.model.Article;
import com.news.newsatoz.viewmodel.ArticleDetailViewModel;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;


public class ArticleDetailActivity extends AppCompatActivity {

    private static final String EXTRAS_ARTICLE = "EXTRAS_ARTICLE";

    private ArticleDetailLayoutBinding articleDetailLayoutBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        articleDetailLayoutBinding =
                DataBindingUtil.setContentView(this, R.layout.article_detail_layout);
        displayHomeAsUpEnabled();
        getExtrasFromIntent();
    }

    public static Intent launchDetail(Context context, String news) {
        Intent intent = new Intent(context, ArticleDetailActivity.class);
        intent.putExtra(EXTRAS_ARTICLE, news);
        return intent;
    }

    private void displayHomeAsUpEnabled() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void getExtrasFromIntent() {
        String newsStr = getIntent().getStringExtra(EXTRAS_ARTICLE);
        Article article = new Gson().fromJson(newsStr, Article.class);
        ArticleDetailViewModel articleDetailViewModel = new ArticleDetailViewModel(article, this);
        articleDetailLayoutBinding.setArticleViewModel(articleDetailViewModel);
        setTitle(articleDetailViewModel.getTitle());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
