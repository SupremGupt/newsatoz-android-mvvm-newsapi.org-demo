package com.news.newsatoz.activities;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.news.newsatoz.NewsApplication;
import com.news.newsatoz.R;
import com.news.newsatoz.adapter.NewsMainListAdapter;
import com.news.newsatoz.databinding.ActivitySearchBinding;
import com.news.newsatoz.model.Article;
import com.news.newsatoz.viewmodel.FeedViewModel;

public class SearchActivity extends AppCompatActivity {

    ActivitySearchBinding mActivitySearchBinding;
    private NewsMainListAdapter adapter;
    private FeedViewModel feedViewModel;
    private String mSearchQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupBindings(savedInstanceState);
        displayHomeAsUpEnabled();

        mActivitySearchBinding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearhResult(query);
                mSearchQuery = query;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    public String getSearchQuery() {
        return mSearchQuery;
    }

    private void callSearhResult(String query) {
        feedViewModel = ViewModelProviders.of(this).get(FeedViewModel.class);
        feedViewModel.init(query);

        mActivitySearchBinding.rvSearch.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new NewsMainListAdapter(this, feedViewModel);

        feedViewModel.getArticleLiveData().observe(this, pagedList -> {
            adapter.submitList(pagedList);
        });

        feedViewModel.getNetworkState().observe(this, networkState -> {
            adapter.setNetworkState(networkState);
        });

        mActivitySearchBinding.rvSearch.setAdapter(adapter);
        setupListClick();
    }

    private void displayHomeAsUpEnabled() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setupBindings(Bundle savedInstanceState) {
        mActivitySearchBinding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        setTitle(R.string.action_search);
    }

    private void setupListClick() {
        feedViewModel.getSelected().observe(this, new Observer<Article>() {
            @Override
            public void onChanged(Article results) {
                if (results != null) {
                    startActivity(ArticleDetailActivity.launchDetail(NewsApplication.getInstance(), new Gson().toJson(results, Article.class)));

                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
