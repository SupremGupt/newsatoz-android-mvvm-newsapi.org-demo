package com.news.newsatoz.datasource;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;

import com.news.newsatoz.NewsApplication;
import com.news.newsatoz.constants.AppConstants;
import com.news.newsatoz.model.Article;
import com.news.newsatoz.model.NewsData;
import com.news.newsatoz.utils.AppUtils;
import com.news.newsatoz.utils.DateUtils;
import com.news.newsatoz.utils.IpLocationUtils;
import com.news.newsatoz.utils.NetworkState;
import com.news.newsatoz.utils.NewsDataHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FeedDataSource extends PageKeyedDataSource<Long, Article> {

    private static final String TAG = FeedDataSource.class.getSimpleName();

    private MutableLiveData networkState;
    private MutableLiveData initialLoading;
    private String mQuery;

    public FeedDataSource(String query) {

        networkState = new MutableLiveData();
        initialLoading = new MutableLiveData();
        mQuery = query;
    }


    public MutableLiveData getNetworkState() {
        return networkState;
    }

    public MutableLiveData getInitialLoading() {
        return initialLoading;
    }


    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<Long, Article> callback) {

        initialLoading.postValue(NetworkState.LOADING);
        networkState.postValue(NetworkState.LOADING);
        if (TextUtils.isEmpty(mQuery)) {
            if (NewsDataHelper.getInstance().shouldGetSavedNewsData()) {
                NewsData newsData = NewsDataHelper.getInstance().getSavedNewsForHeadline();
                if (newsData != null) {
                    callback.onResult(newsData.getArticles(), null, 2l);
                    initialLoading.postValue(NetworkState.LOADED);
                    networkState.postValue(NetworkState.LOADED);
                    return;
                }
            }

            NewsApplication.getInstance().getRestApi().getTopHeadLines(IpLocationUtils.getUserCountryCode(), "popularity", AppConstants.NEWS_API_KEY, 1)
                    .enqueue(new Callback<NewsData>() {
                        @Override
                        public void onResponse(Call<NewsData> call, Response<NewsData> response) {
                            if (response.isSuccessful()) {
                                NewsDataHelper.getInstance().saveNewsDataForHeadline(response.body());
                                callback.onResult(response.body().getArticles(), null, 2l);
                                initialLoading.postValue(NetworkState.LOADED);
                                networkState.postValue(NetworkState.LOADED);

                            } else {
                                initialLoading.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                            }
                        }

                        @Override
                        public void onFailure(Call<NewsData> call, Throwable t) {
                            String errorMessage = t == null ? "unknown error" : t.getMessage();
                            networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorMessage));
                        }
                    });
        } else {
            getSearchedNews(params, callback, mQuery);
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, Article> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, Article> callback) {
        if (TextUtils.isEmpty(mQuery)) {
            networkState.postValue(NetworkState.LOADING);
            getAllNews(params, callback);
        } else {
            /*do not load more results in case of search*/
            networkState.postValue(NetworkState.LOADED);
        }

    }


    private void getAllNews(LoadParams<Long> params, LoadCallback<Long, Article> callback) {
        if (NewsDataHelper.getInstance().shouldGetSavedNewsData()) {
            NewsData newsData = NewsDataHelper.getInstance().getSavedNewsForPage((int) (params.key - 1));
            if (newsData != null) {
                long nextKey = (params.key == newsData.getTotalResults()) ? null : params.key + 1;
                callback.onResult(newsData.getArticles(), nextKey);
                networkState.postValue(NetworkState.LOADED);
                return;
            }
        }
        NewsApplication.getInstance().getRestApi().getAllList("Top News", "popularity", DateUtils.getOneMonthBeforeDate(), DateUtils.getTodayDate(),
                AppConstants.NEWS_API_KEY, params.key - 1, params.requestedLoadSize).enqueue(new Callback<NewsData>() {
            @Override
            public void onResponse(Call<NewsData> call, Response<NewsData> response) {
                if (response.isSuccessful()) {
                    NewsDataHelper.getInstance().saveNewsDataForPage(response.body(), (int) (params.key - 1));
                    NewsDataHelper.getInstance().setLastSaveNewsTime();

                    long nextKey = (params.key == response.body().getTotalResults()) ? null : params.key + 1;
                    callback.onResult(response.body().getArticles(), nextKey);
                    networkState.postValue(NetworkState.LOADED);

                } else {
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                }
            }

            @Override
            public void onFailure(Call<NewsData> call, Throwable t) {
                String errorMessage = t == null ? "unknown error" : t.getMessage();
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorMessage));
            }
        });
    }

    private void getSearchedNews(LoadInitialParams<Long> params, LoadInitialCallback<Long, Article> callback, String query) {
        if (!AppUtils.isNetworkConnected()) {
            NewsData newsData = NewsDataHelper.getInstance().getSavedSearchData();
            if (newsData != null) {
                callback.onResult(newsData.getArticles(), null, 2l);
                networkState.postValue(NetworkState.LOADED);
            }
        }

        NewsApplication.getInstance().getRestApi().getAllList(query, "popularity", DateUtils.getOneMonthBeforeDate(), DateUtils.getTodayDate(),
                AppConstants.NEWS_API_KEY, 1).enqueue(new Callback<NewsData>() {
            @Override
            public void onResponse(Call<NewsData> call, Response<NewsData> response) {
                if (response.isSuccessful()) {
                    callback.onResult(response.body().getArticles(), null, 2l);
                    NewsDataHelper.getInstance().saveSearchData(response.body());
                    NewsDataHelper.getInstance().setLastSaveSearchTime();
                    networkState.postValue(NetworkState.LOADED);

                } else {
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                }
            }

            @Override
            public void onFailure(Call<NewsData> call, Throwable t) {
                String errorMessage = t == null ? "unknown error" : t.getMessage();
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorMessage));
            }
        });
    }
}
