package com.news.newsatoz.datasource.factory;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import com.news.newsatoz.datasource.FeedDataSource;

public class FeedDataFactory extends DataSource.Factory {

    private MutableLiveData<FeedDataSource> mutableLiveData;
    private FeedDataSource feedDataSource;
    private String mQuery = "";

    public FeedDataFactory(String query) {
        this.mutableLiveData = new MutableLiveData<FeedDataSource>();
        this.mQuery = query;
    }


    @Override
    public DataSource create() {
        feedDataSource = new FeedDataSource(mQuery);
        mutableLiveData.postValue(feedDataSource);
        return feedDataSource;
    }


    public MutableLiveData<FeedDataSource> getMutableLiveData() {
        return mutableLiveData;
    }
}
