package com.news.newsatoz.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.databinding.BaseObservable;

public class IpLocation extends BaseObservable {

    /**
     * ip : 106.51.80.195
     * type : ipv4
     * continent_code : AS
     * continent_name : Asia
     * country_code : IN
     * country_name : India
     * region_code : KA
     * region_name : Karnataka
     * city : Bengaluru
     * zip : 560076
     * latitude : 12.9833
     * longitude : 77.5833
     * location : {"geoname_id":1277333,"capital":"New Delhi","languages":[{"code":"hi","name":"Hindi","native":"हिन्दी"},{"code":"en","name":"English","native":"English"}],"country_flag":"http://assets.ipstack.com/flags/in.svg","country_flag_emoji":"🇮🇳","country_flag_emoji_unicode":"U+1F1EE U+1F1F3","calling_code":"91","is_eu":false}
     */

    private String ip;
    private String type;
    private String continent_code;
    private String continent_name;
    private String country_code;
    private String country_name;
    private String region_code;
    private String region_name;
    private String city;
    private String zip;
    private double latitude;
    private double longitude;
    private Location location;

    public static IpLocation objectFromData(String str) {

        return new Gson().fromJson(str, IpLocation.class);
    }

    public static IpLocation objectFromData(String str, String key) {

        try {
            JSONObject jsonObject = new JSONObject(str);

            return new Gson().fromJson(jsonObject.getString(str), IpLocation.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<IpLocation> arrayIpLocationFromData(String str) {

        Type listType = new TypeToken<ArrayList<IpLocation>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public static List<IpLocation> arrayIpLocationFromData(String str, String key) {

        try {
            JSONObject jsonObject = new JSONObject(str);
            Type listType = new TypeToken<ArrayList<IpLocation>>() {
            }.getType();

            return new Gson().fromJson(jsonObject.getString(str), listType);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new ArrayList();


    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContinent_code() {
        return continent_code;
    }

    public void setContinent_code(String continent_code) {
        this.continent_code = continent_code;
    }

    public String getContinent_name() {
        return continent_name;
    }

    public void setContinent_name(String continent_name) {
        this.continent_name = continent_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getRegion_code() {
        return region_code;
    }

    public void setRegion_code(String region_code) {
        this.region_code = region_code;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


    public static class Languages {
        /**
         * code : hi
         * name : Hindi
         * native : हिन्दी
         */

        private String code;
        private String name;
        @SerializedName("native")
        private String nativeX;

        public static Languages objectFromData(String str) {

            return new Gson().fromJson(str, Languages.class);
        }

        public static Languages objectFromData(String str, String key) {

            try {
                JSONObject jsonObject = new JSONObject(str);

                return new Gson().fromJson(jsonObject.getString(str), Languages.class);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        public static List<Languages> arrayLanguagesFromData(String str) {

            Type listType = new TypeToken<ArrayList<Languages>>() {
            }.getType();

            return new Gson().fromJson(str, listType);
        }

        public static List<Languages> arrayLanguagesFromData(String str, String key) {

            try {
                JSONObject jsonObject = new JSONObject(str);
                Type listType = new TypeToken<ArrayList<Languages>>() {
                }.getType();

                return new Gson().fromJson(jsonObject.getString(str), listType);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return new ArrayList();


        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNativeX() {
            return nativeX;
        }

        public void setNativeX(String nativeX) {
            this.nativeX = nativeX;
        }
    }

    public static class Location {
        /**
         * geoname_id : 1277333
         * capital : New Delhi
         * languages : [{"code":"hi","name":"Hindi","native":"हिन्दी"},{"code":"en","name":"English","native":"English"}]
         * country_flag : http://assets.ipstack.com/flags/in.svg
         * country_flag_emoji : 🇮🇳
         * country_flag_emoji_unicode : U+1F1EE U+1F1F3
         * calling_code : 91
         * is_eu : false
         */

        private int geoname_id;
        private String capital;
        private String country_flag;
        private String country_flag_emoji;
        private String country_flag_emoji_unicode;
        private String calling_code;
        private boolean is_eu;
        private List<Languages> languages;

        public static Location objectFromData(String str) {

            return new Gson().fromJson(str, Location.class);
        }

        public static Location objectFromData(String str, String key) {

            try {
                JSONObject jsonObject = new JSONObject(str);

                return new Gson().fromJson(jsonObject.getString(str), Location.class);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        public static List<Location> arrayLocationFromData(String str) {

            Type listType = new TypeToken<ArrayList<Location>>() {
            }.getType();

            return new Gson().fromJson(str, listType);
        }

        public static List<Location> arrayLocationFromData(String str, String key) {

            try {
                JSONObject jsonObject = new JSONObject(str);
                Type listType = new TypeToken<ArrayList<Location>>() {
                }.getType();

                return new Gson().fromJson(jsonObject.getString(str), listType);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return new ArrayList();


        }

        public int getGeoname_id() {
            return geoname_id;
        }

        public void setGeoname_id(int geoname_id) {
            this.geoname_id = geoname_id;
        }

        public String getCapital() {
            return capital;
        }

        public void setCapital(String capital) {
            this.capital = capital;
        }

        public String getCountry_flag() {
            return country_flag;
        }

        public void setCountry_flag(String country_flag) {
            this.country_flag = country_flag;
        }

        public String getCountry_flag_emoji() {
            return country_flag_emoji;
        }

        public void setCountry_flag_emoji(String country_flag_emoji) {
            this.country_flag_emoji = country_flag_emoji;
        }

        public String getCountry_flag_emoji_unicode() {
            return country_flag_emoji_unicode;
        }

        public void setCountry_flag_emoji_unicode(String country_flag_emoji_unicode) {
            this.country_flag_emoji_unicode = country_flag_emoji_unicode;
        }

        public String getCalling_code() {
            return calling_code;
        }

        public void setCalling_code(String calling_code) {
            this.calling_code = calling_code;
        }

        public boolean isIs_eu() {
            return is_eu;
        }

        public void setIs_eu(boolean is_eu) {
            this.is_eu = is_eu;
        }

        public List<Languages> getLanguages() {
            return languages;
        }

        public void setLanguages(List<Languages> languages) {
            this.languages = languages;
        }
    }

}
